This repository contains modem firmware for the Google Pixel 3a, reformatted to
be used by the [SDM670 Linux kernel](https://gitlab.com/sdm670-mainline/linux).
It is not specifically endorsed by Google in any way.

# Credits

The modem firmware is provided by Google. See the [Terms and
conditions](https://developers.google.com/android/ota#legal).

# Extraction

The [python-protobuf](https://pypi.org/project/protobuf/) and
[pil-squasher](https://github.com/andersson/pil-squasher) packages are required
to extract the modem firmware from the official OTA image.

    $ curl -O https://dl.google.com/dl/android/aosp/sargo-ota-sp2a.220505.008-2037245c.zip
    $ git clone https://github.com/LineageOS/android_tools_extract-utils android/tools/extract-utils
    $ git clone https://github.com/LineageOS/android_system_update_engine android/system/update_engine
    $ unzip sargo-ota-sp2a.220505.008-2037245c.zip payload.bin
    $ android/tools/extract-utils/extract_ota.py -p modem -o . payload.bin
    # mount -o ro modem.img mnt/
    $ pil-squasher modemfw/modem.mbn mnt/image/modem.mdt
    $ install -m644 mnt/image/modemr.jsn mnt/image/mba.mbn modemfw/
    # umount mnt/
